package com.sky.livecontacts

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_contact_detail.*

/**
 * Created by skyalligator on 1/16/19.
 * 9:34 PM
 */
class ContactDetailActivity : AppCompatActivity() {

    companion object {
        val KEY_URI = "uri"
        val KEY_NAME = "name"
        val KEY_NO = "no"
        val KEY_ONLINE = "online"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_detail)
        setSupportActionBar(ui_detailAct_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        intent.extras?.let { bundle ->
            val phNo = bundle.getString(KEY_NO)

            ui_item_Detail_messageV.setOnClickListener {
                sendSMSMessage("Hi, I am using Live Contacts App :)",phNo)
            }

            Picasso.get().load(bundle.getString(KEY_URI)).placeholder(R.drawable.contact_icon).into(ui_frag_detail_imgV)

            ui_frag_detail_name.setNullableText(bundle.getString(KEY_NAME))
            ui_frag_detail_no.setNullableText(phNo)
            if (bundle.getBoolean(KEY_ONLINE))
                ui_item_Detail_onlineV.setImageResource(R.drawable.online_circle)
            else
                ui_item_Detail_onlineV.setImageResource(R.drawable.offline_circle)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

}