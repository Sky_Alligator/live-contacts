package com.sky.livecontacts

import android.Manifest
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_contacts.*


class ContactsActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    val PERMISSION_REQUEST_CONTACT = 21

    var adapter: ListAdapter? = null
    private val contactsDb = FirebaseDatabase.getInstance().getReference("contacts")
    private var listV: RecyclerView? = null
    private var contactsMap: Map<String, Contact> = emptyMap()

    var listener: ValueEventListener = object : ValueEventListener {

        override fun onCancelled(err: DatabaseError) {
            err.print()
        }

        override fun onDataChange(snapshot: DataSnapshot) {
            updateOnlineStatus(snapshot.value.toString())
        }

        fun updateOnlineStatus(data: String) {
            parseFirebaseData(data)
        }

        private fun parseFirebaseData(data: String) {
            ignoreEx {
                val phNos = data.replace("{", "").replace("}", "").split(",")
                phNos.forEach {
                    ignoreEx {
                        val split = it.split("=")
                        contactsMap[split[0].trim()]?.status = split[1].toBoolean()
                    }
                }
                listV?.adapter?.notifyDataSetChanged()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_contacts, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu?.findItem(R.id.action_search)?.actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            isSubmitButtonEnabled = true;
            setOnQueryTextListener(this@ContactsActivity)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {

            R.id.action_sync -> initContacts()

            R.id.action_sighOut -> {
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(this, AuthActivity::class.java))
                finish()
            }
        }
        return true
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(f: String?): Boolean {
        if (f.isNullOrBlank())
            adapter?.clearFilter()
        else
            adapter?.filterList(f)
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)
        setSupportActionBar(toolbar)
        actionBar?.setDisplayHomeAsUpEnabled(false)

        requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), PERMISSION_REQUEST_CONTACT)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CONTACT -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initContacts()
                    contactsDb.removeEventListener(listener)
                    contactsDb.addValueEventListener(listener)
                }
                else {
                    Toast.makeText(this, "No permission for contacts", Toast.LENGTH_SHORT).show()
                    finish()
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onStart() {
        super.onStart()
        setStatus(true)
        contactsDb.addValueEventListener(listener)
    }

    override fun onDestroy() {
        super.onDestroy()
        setStatus(false)
        contactsDb.removeEventListener(listener)
    }

    private fun initContacts() {
        contactsMap = getContacts(this)

        val layoutManager = LinearLayoutManager(this)
        contactsList.layoutManager = layoutManager

        adapter = ListAdapter(contactsMap, layoutManager) {
            val intent = Intent(this@ContactsActivity, ContactDetailActivity::class.java)
            intent.putExtra(ContactDetailActivity.KEY_NAME, it.name)
            intent.putExtra(ContactDetailActivity.KEY_NO, it.phNo)
            intent.putExtra(ContactDetailActivity.KEY_URI, it.photoUri)
            intent.putExtra(ContactDetailActivity.KEY_ONLINE, it.status)
            startActivity(intent)
        }
        contactsList.adapter = adapter

    }

    private fun setStatus(status: Boolean) {
        FirebaseAuth.getInstance().currentUser?.phoneNumber?.let {
            contactsDb.child(it).setValue(status)
        }
    }
}
