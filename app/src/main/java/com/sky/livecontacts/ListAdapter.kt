package com.sky.livecontacts

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_contact_list.view.*


/**
 * Created by skyalligator on 1/10/19.
 * 10:48 PM
 */
class ListAdapter(map: Map<String, Contact>, val lm: LinearLayoutManager, val listener: (Contact) -> Unit) :
    RecyclerView.Adapter<ContactsHolder>() {

    private var list: List<Contact> = map.values.toList()
    private var originalList: List<Contact>
    private val comparator = compareBy<Contact> { it.name }

    init {
        list = list.sortedWith(comparator)
        setLabel()
        originalList = list
    }

    private fun setLabel() {
        var prevChar = ' '
        for (contact in list) {
            val c = contact.name[0]
            if (c != prevChar) {
                prevChar = c
                contact.label = c.toString()
            }
        }
    }

    fun filterList(search: String) {
        list = originalList.filter {
            it.name.contains(search, true)
        }.sortedWith(comparator)
        notifyDataSetChanged()
    }

    fun clearFilter() {
        list = originalList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ContactsHolder {
        val personCell = LayoutInflater.from(parent.context).inflate(R.layout.item_contact_list, parent, false)
        return ContactsHolder(personCell, listener)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ContactsHolder, i: Int) {
        holder.setValues(list[i])
    }
}

class ContactsHolder(itemV: View, val listener: (Contact) -> Unit) : RecyclerView.ViewHolder(itemV) {

    fun setValues(data: Contact) {
        Picasso.get().load(data.photoUri).placeholder(R.drawable.contact_icon).into(itemView.ui_item_contact_imgV)

        if (!data.label.isEmpty())
            itemView.imageView.visibility = View.VISIBLE
        else
            itemView.imageView.visibility = View.INVISIBLE

        itemView.ui_item_contact_labelTv.setNullableText(data.label)
        itemView.ui_item_contact_nameTv.setNullableText(data.name)
        itemView.ui_item_contact_phV.setNullableText(data.phNo)

        if (data.status)
            itemView.ui_item_contact_onlineV.setImageResource(R.drawable.online_circle)
        else
            itemView.ui_item_contact_onlineV.setImageResource(R.drawable.offline_circle)

        itemView.setOnClickListener {
            listener(data)
        }
    }
}