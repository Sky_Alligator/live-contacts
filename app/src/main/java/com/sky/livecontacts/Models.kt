package com.sky.livecontacts


/**
 * Created by skyalligator on 1/10/19.
 * 10:42 PM
 */
data class Contact(
    val phNo: String? = "",
    var status: Boolean = false,
    val name: String = "",
    val photoUri: String? = null,
    var label: String = ""
)