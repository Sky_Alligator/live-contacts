package com.sky.livecontacts

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.ContactsContract
import android.util.Log
import android.widget.TextView
import android.net.Uri.fromParts
import android.support.v4.content.ContextCompat.startActivity



/**
 * Created by skyalligator on 1/16/19.
 * 5:36 PM
 */

val TAG = "AppLog"

fun Any?.print() {
    Log.d(TAG, this?.toString() ?: "null Object")
}

fun TextView.setNullableText(txt: String?) {
    txt?.let {
        text = it
    }
}

fun ignoreEx(fnc:()->Unit){
    try {
        fnc()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun getContacts(ctx: Context): HashMap<String, Contact> {
    val list = HashMap<String, Contact>()

    val projection = arrayOf(
        ContactsContract.Contacts._ID,
        ContactsContract.Contacts.DISPLAY_NAME,
        ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER,
        ContactsContract.Contacts.PHOTO_THUMBNAIL_URI
    )

    ctx.contentResolver.query(
        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection,
        null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC"
    )?.let { data ->

        while (data.moveToNext()) {
            val id = data.getString(0)
            val name = data.getString(1)
            val number = data.getString(2)
            val thumbnailUri = data.getString(3)


            if (number != null) {
                list[number] = Contact(name = name, phNo = number, photoUri = thumbnailUri)
                number.print()
            }
        }
        data.close()
    }
    return list
}

fun Activity.sendSMSMessage(message: String,no: String) {

    val intent = Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", no, null))
    intent.putExtra("sms_body",message)
    startActivity(intent)
}
