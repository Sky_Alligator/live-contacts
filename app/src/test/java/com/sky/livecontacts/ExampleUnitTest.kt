package com.sky.livecontacts

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.junit.Assert
import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun objectToJson() {
        val contactList = arrayListOf(
            Contact("+919790785998", true),
            Contact("+919790785999", false),
            Contact("+919790785922", true),
            Contact("+919790785934", false)
        )
        val jsonString = Gson().toJson(contactList)
        Assert.assertEquals( """[{"phNo":"+919790785998","status":true},{"phNo":"+919790785999","status":false},{"phNo":"+919790785922","status":true},{"phNo":"+919790785934","status":false}]""",jsonString)
    }

    @Test
    fun jsonToObject() {

        val json = """[{"phNo":"+919790785998","status":true},{"phNo":"+919790785999","status":false},{"phNo":"+919790785922","status":true},{"phNo":"+919790785934","status":false}]"""
        val contactList = arrayListOf(
            Contact("+919790785998", true),
            Contact("+919790785999", false),
            Contact("+919790785922", true),
            Contact("+919790785934", false)
        )
        val turnsType = object : TypeToken<List<Contact>>() {}.type

        val obj = Gson().fromJson<List<Contact>>(json,turnsType)
        Assert.assertEquals(contactList, obj)
    }

}
